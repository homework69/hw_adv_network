import socket

devices_list = []
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as socket_:
    socket_ = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    socket_.bind(('', 8888))

    while True:
        result = socket_.recv(1024)
        client_id = result.decode('utf-8')
        if client_id == 'Stop':
            print("Command 'Stop' received!")
            break
        else:
            if client_id not in devices_list:
                print(f"New device appeared, UID: {client_id}")
                devices_list.append(client_id)
            else:
                pass
                # print(f"This device is already exist,  UID: {client_id}")

# print(devices_list)
# print(len(devices_list))
