import socket
import random

uid_base = 'aabb-16cc-18dd-'
uid_gen = (uid_base + str(random.randint(1000, 1100)) for _ in range(100))

with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
    for uid in uid_gen:
        sock.sendto(uid.encode('utf-8'), ('localhost', 8888))
    sock.sendto(b'Stop', ('localhost', 8888))
